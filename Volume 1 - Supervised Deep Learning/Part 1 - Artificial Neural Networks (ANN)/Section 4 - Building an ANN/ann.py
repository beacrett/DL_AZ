# Artificial Neural Network

# Data Preprossesing

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Churn_Modelling.csv')
X = dataset.iloc[:, 3:13].values
y = dataset.iloc[:, 13].values

# Encoding categorical data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X_1 = LabelEncoder()
X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])

labelencoder_X_2 = LabelEncoder()
X[:, 2] = labelencoder_X_2.fit_transform(X[:, 2])

onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:, 1:]    # remove a variable from the dummy vars to avoid the dummy var trap

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Fitting classifier to the Training set
# Create your classifier here

# one way to force tensorflow to use cpu over gpu - note before keras import
# import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# second way to tell tensorflow to use cpu -> tell it how many threads, devices etc
import tensorflow as tf
import keras
from keras import backend as K
num_cores = 6
num_gpu = 0
num_cpu = 1
config = tf.ConfigProto(intra_op_parallelism_threads=num_cores, 
                        inter_op_parallelism_threads=num_cores,
                        allow_soft_placement=True,
                        device_count={'CPU': num_cpu, 'GPU': num_gpu})
session = tf.Session(config=config)
K.set_session(session)



from keras.models import Sequential
from keras.layers import Dense

# init the ANN
classifier = Sequential()

# add layers (adds the input and first hidden layer)
classifier.add(Dense(input_dim=11, output_dim=6, init='uniform', activation='relu'))
# add a second hiden layer
classifier.add(Dense(output_dim=6, init='uniform', activation='relu'))
# add output layer (sigmoid good for single dim, softmax is multi dim equiv)
classifier.add(Dense(output_dim=1, init='uniform', activation='sigmoid'))

# compiling ANN
classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

# fit ann
classifier.fit(X_train, y_train, batch_size=1000, epochs=100)

# Predicting the Test set results
y_pred = classifier.predict(X_test)
y_pred = (y_pred > 0.5)

# Making the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)

# hw
hw_X = np.array([[0, 0, 600, 1, 40, 3, 60000, 2, 1, 1, 50000]], dtype=np.float64)
hw_X = sc.transform(hw_X)
hw_pred = classifier.predict(hw_X)
hw_pred = (hw_pred > 0.5)

